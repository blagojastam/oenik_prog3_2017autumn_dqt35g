var searchData=
[
  ['damageincrease',['DAMAGEINCREASE',['../namespace_captain_evil.html#aa209a9a2f184e79d9a3c68f10e4c5694a64a47f26251eb84f53c99ec9b4a9c2cc',1,'CaptainEvil']]],
  ['damageresilience',['DAMAGERESILIENCE',['../namespace_captain_evil.html#aa209a9a2f184e79d9a3c68f10e4c5694abdd10a22d5ff2bce63cf55cfd3ee03d4',1,'CaptainEvil']]],
  ['deactivatepowerup',['DeactivatePowerup',['../class_captain_evil_1_1_power_up.html#a1f8e27b69a28eb71ab26d94d2b921032',1,'CaptainEvil::PowerUp']]],
  ['deltax',['DeltaX',['../class_captain_evil_1_1_game_object.html#a5cd4e27dc8ddd7f255986106bdd6d0bb',1,'CaptainEvil::GameObject']]],
  ['deltay',['DeltaY',['../class_captain_evil_1_1_game_object.html#a7028accc2c187fe8c06ee643049f7c57',1,'CaptainEvil::GameObject']]],
  ['duration',['Duration',['../class_captain_evil_1_1_power_up.html#a32386947a1d3763ab97529506523c107',1,'CaptainEvil::PowerUp']]]
];

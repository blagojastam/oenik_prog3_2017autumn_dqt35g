﻿namespace CaptainEvil
{
    using System.Collections.Generic;

    /// <summary>
    /// Abstract class for inheritance purposes
    /// </summary>
    public abstract class PlayerCharacter : MoveableGameObject
    {
        /// <summary>
        /// String for indicating the player name
        /// </summary>
        private string playerName;

        /// <summary>
        /// The maximum horizontal speed of the player character
        /// </summary>
        private double maxSpeed;

        /// <summary>
        /// The maximum health of the player character
        /// </summary>
        private double maxHealth;

        /// <summary>
        /// Total pistol ammo that the player currently has. 
        /// </summary>
        private int pistolAmmo;

        /// <summary>
        /// Total rifle ammo that the player currently has. 
        /// </summary>
        private int rifleAmmo;

        /// <summary>
        /// List of PowerUps of the player
        /// </summary>
        private List<PowerUp> powerUps;

        /// <summary>
        /// Pistol of the player
        /// </summary>
        private Pistol pistol;

        /// <summary>
        /// Rifle of the player
        /// </summary>
        private Rifle rifle;

        /// <summary>
        /// The currently selected weapon of the player
        /// </summary>
        private Weapon selectedWeapon;

        /// <summary>
        /// List of Coins of the player
        /// </summary>
        private List<Coin> coins;

        /// <summary>
        /// Gets or sets the name of the player
        /// </summary>
        public string PlayerName
        {
            get
            {
                return this.playerName;
            }

            set
            {
                this.playerName = value;
            }
        }

        /// <summary>
        /// Gets or sets the maximum speed of the player
        /// </summary>
        public double MaxSpeed
        {
            get
            {
                return this.maxSpeed;
            }

            set
            {
                this.maxSpeed = value;
            }
        }

        /// <summary>
        /// Gets or sets the maximum health of the player
        /// </summary>
        public double MaxHealth
        {
            get
            {
                return this.maxHealth;
            }

            set
            {
                this.maxHealth = value;
            }
        }

        /// <summary>
        /// Gets or sets the total pistol ammo the player has.
        /// </summary>
        public int PistolAmmo
        {
            get
            {
                return this.pistolAmmo;
            }

            set
            {
                this.pistolAmmo = value;
            }
        }

        /// <summary>
        /// Gets or sets the total rifle ammo the player has.
        /// </summary>
        public int RifleAmmo
        {
            get
            {
                return this.rifleAmmo;
            }

            set
            {
                this.rifleAmmo = value;
            }
        }

        /// <summary>
        /// Gets or sets the pistol of the player
        /// </summary>
        public Pistol Pistol
        {
            get
            {
                return this.pistol;
            }

            set
            {
                this.pistol = value;
            }
        }

        /// <summary>
        /// Gets or sets the rifle of the player
        /// </summary>
        public Rifle Rifle
        {
            get
            {
                return this.rifle;
            }

            set
            {
                this.rifle = value;
            }
        }

        /// <summary>
        /// Gets or sets the currently selected weapon of the player
        /// </summary>
        public Weapon SelectedWeapon
        {
            get
            {
                return this.selectedWeapon;
            }

            set
            {
                this.selectedWeapon = value;
            }
        }

        /// <summary>
        /// Gets or sets the list of powerups of the player
        /// </summary>
        public List<PowerUp> PowerUps
        {
            get
            {
                return this.powerUps;
            }

            set
            {
                this.powerUps = value;
            }
        }

        /// <summary>
        /// Gets or sets the list of coins of the player
        /// </summary>
        internal List<Coin> Coins
        {
            get
            {
                return this.coins;
            }

            set
            {
                this.coins = value;
            }
        }

        /// <summary>
        /// Switches the selected weapon of the player
        /// </summary>
        /// <param name="weaponID">ID of the weapon</param>
        public void SelectWeapon(int weaponID)
        {
            if (weaponID == 2 && this.pistol != null)
            {
                this.selectedWeapon = this.pistol;
            }

            if (weaponID == 1 && this.rifle != null)
            {
                this.selectedWeapon = this.rifle;
            }
        }

        /// <summary>
        /// Activates the powerups 
        /// </summary>
        /// <param name="boost">The powerup to be activated</param>
        public void ActivatePowerup(PowerUp boost)
        {
            if (this.PowerUps.Contains(boost))
            {
                this.PowerUps.Remove(boost);
                boost.IsActivated = true;
                switch (boost.Type)
                {
                    case PowerUpType.NOTHING:
                        break;
                    case PowerUpType.INVINCIBILITY:
                        break;
                    case PowerUpType.SPEEDINCREASE:
                        this.MaxSpeed *= 135;
                        break;
                    case PowerUpType.DAMAGERESILIENCE:
                        break;
                    case PowerUpType.INFINITEAMMO:
                        if (this.Pistol != null)
                        {
                            this.Pistol.CurrentMagazineAmmo = int.MaxValue;
                        }

                        if (this.Rifle != null)
                        {
                            this.Rifle.CurrentMagazineAmmo = int.MaxValue;
                        }

                        break;
                    case PowerUpType.DAMAGEINCREASE:
                        break;
                    default:
                        break;
                }
            }
        }
    }
}

﻿namespace CaptainEvil
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Class defining the Alice player character
    /// </summary>
    public class Alice : PlayerCharacter
    {
        /// <summary>
        /// Initializes a new instance of the Alice class. 
        /// </summary>
        /// <param name="coorX">The X coordinate where the instance will be created</param>
        /// <param name="coorY">The Y coordinate where the instance will be created</param>
        public Alice(int coorX, int coorY)
        {
            this.BrushLocation = new Uri("../../Resources/ImageBrushes/Alice.png", UriKind.Relative);
            this.MaxHealth = Config.BOBHEALTH; // CHANGE TO ALICE
            this.MaxSpeed = Config.BOBSPEED; // ALICE
            this.Pistol = new Pistol();
            this.PistolAmmo = 40;
            this.PowerUps = new List<PowerUp>();
            this.Coins = new List<Coin>();
            Rect r1 = new Rect(coorX, coorY, 40, 80);
            this.ObjectArea = new RectangleGeometry(r1);
        }
    }
}

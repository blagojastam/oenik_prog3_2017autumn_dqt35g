﻿namespace CaptainEvil
{
    /// <summary>
    /// Abstract NPC class for inheritance purposes
    /// </summary>
    public abstract class NPC : MoveableGameObject
    {
        /// <summary>
        /// The maximum horizontal speed of the NPC
        /// </summary>
        private double maxSpeed;

        /// <summary>
        /// The maximum health of the NPC
        /// </summary>
        private double maxHealth;

        /// <summary>
        /// Gets or sets the maximum speed of the NPC
        /// </summary>
        public double MaxSpeed
        {
            get
            {
                return this.maxSpeed;
            }

            set
            {
                this.maxSpeed = value;
            }
        }

        /// <summary>
        /// Gets or sets the maximum health of the NPC
        /// </summary>
        public double MaxHealth
        {
            get
            {
                return this.maxHealth;
            }

            set
            {
                this.maxHealth = value;
            }
        }
    }
}

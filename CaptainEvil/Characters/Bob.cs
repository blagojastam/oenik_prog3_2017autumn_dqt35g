﻿namespace CaptainEvil
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Class defining the Bob player character
    /// </summary>
    public class Bob : PlayerCharacter
    {
        /// <summary>
        /// Initializes a new instance of the Bob class. 
        /// </summary>
        /// <param name="coorX">The X coordinate where the instance will be created</param>
        /// <param name="coorY">The Y coordinate where the instance will be created</param>
        public Bob(int coorX, int coorY)
        {
            this.BrushLocation = new Uri("../../Resources/ImageBrushes/Bob.png", UriKind.Relative);
            this.MaxHealth = Config.BOBHEALTH;
            this.MaxSpeed = Config.BOBSPEED;
            this.Pistol = new Pistol();
            this.PistolAmmo = 40;
            this.SelectedWeapon = this.Pistol;
            this.Rifle = new Rifle();
            this.RifleAmmo = 60;
            this.PowerUps = new List<PowerUp>();
            this.Coins = new List<Coin>();
            Rect r1 = new Rect(coorX, coorY, 40, 80);
            this.ObjectArea = new RectangleGeometry(r1);
        }
    }
}

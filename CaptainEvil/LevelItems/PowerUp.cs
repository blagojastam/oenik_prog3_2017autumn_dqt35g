﻿namespace CaptainEvil
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Threading;

    /// <summary>
    /// Class representing powerups
    /// </summary>
    public class PowerUp : ImmoveableGameObject
    {
        /// <summary>
        /// A boolean value indicating whether the item has been picked up.
        /// </summary>
        private bool isPickedUp;

        /// <summary>
        /// A boolean value indicating whether the item has been activated.
        /// </summary>
        private bool isActivated;

        /// <summary>
        /// The duration of the powerup in seconds.
        /// </summary>
        private double duration;

        /// <summary>
        /// The type of the powerup.
        /// </summary>
        private PowerUpType type;

        /// <summary>
        /// The amount of seconds after the powerup has been activated.
        /// </summary>
        private double elapsedSeconds = 0;

        /// <summary>
        /// Gets or sets the value indicating whether the item has been picked up.
        /// </summary>
        public bool IsPickedUp
        {
            get
            {
                return this.isPickedUp;
            }

            set
            {
                this.isPickedUp = value;
            }
        }

        /// <summary>
        /// Gets or sets the value whether the item has been activated.
        /// </summary>
        public bool IsActivated
        {
            get
            {
                return this.isActivated;
            }

            set
            {
                this.isActivated = value;
            }
        }

        /// <summary>
        /// Gets or sets the duration in seconds of the powerup.
        /// </summary>
        public double Duration
        {
            get
            {
                return this.duration;
            }

            set
            {
                this.duration = value;
            }
        }

        /// <summary>
        /// Gets or sets the type of the powerup.
        /// </summary>
        public PowerUpType Type
        {
            get
            {
                return this.type;
            }

            set
            {
                this.type = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the PowerUp class.
        /// </summary>
        /// <param name="type">Type of the powerup</param>
        public PowerUp(PowerUpType type)
        {
            this.Type = type;
            switch (this.Type)
            {
                case PowerUpType.NOTHING:
                    break;
                case PowerUpType.INVINCIBILITY:
                    this.Duration = 10;
                    break;
                case PowerUpType.SPEEDINCREASE:
                    this.Duration = 15; 
                    break;
                case PowerUpType.DAMAGERESILIENCE:
                    this.Duration = 20; 
                    break;
                case PowerUpType.INFINITEAMMO:
                    this.Duration = 10;
                    break;
                case PowerUpType.DAMAGEINCREASE: // consider refactoring cases in the activate method
                    this.Duration = 15; 
                    break;
                default:
                    break;
            }

            this.IsPickedUp = false;
            this.IsActivated = false;
            Rect r1 = new Rect(Config.WINDOWWIDTH - 40, Config.WINDOWHEIGHT - 150, 10, 10);
            this.ObjectArea = new EllipseGeometry(r1);
        }

        /// <summary>
        /// Adds the powerup to the player's collection of powerups.
        /// </summary>
        /// <param name="currentPlayer">The player that interacts with the Powerup</param>
        public void PickUp(PlayerCharacter currentPlayer)
        {
            currentPlayer.PowerUps.Add(this);
            this.isPickedUp = true; 
        }

        /// <summary>
        /// Activates the powerup and its effects on the player.
        /// </summary>
        /// <param name="currentPlayer">The player that interacts with the Powerup</param>
        public void ActivatePowerup(PlayerCharacter currentPlayer)
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(1000 / Config.TICKRATE);
            timer.Tick += this.PowerUpTimerTick;
            timer.Start();
            this.isActivated = true;
            this.isPickedUp = false; 
            if (this.elapsedSeconds > this.duration)
            {
                timer.Stop();
                this.elapsedSeconds = 0;
                this.DeactivatePowerup(currentPlayer, timer);        
            }
        }

        /// <summary>
        /// Deactivates the powerup and its effects on the player
        /// </summary>
        /// <param name="currentPlayer">The player that interacts with the Powerup</param>
        /// <param name="timer">A DispatcherTimer Instance</param>
        public void DeactivatePowerup(PlayerCharacter currentPlayer, DispatcherTimer timer)
        {
            this.isActivated = false; 
        }

        /// <summary>
        /// Method that gets called at each tick of the timer
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Event arguments</param>
        private void PowerUpTimerTick(object sender, EventArgs e)
        {
            this.elapsedSeconds++;
            this.elapsedSeconds /= Config.TICKRATE;
        }
    }
}

﻿namespace CaptainEvil
{
    using System;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Class used to represent coins
    /// </summary>
    public class Coin : ImmoveableGameObject
    {
        /// <summary>
        /// Initializes a new instance of the Coin class.
        /// </summary>
        /// <param name="coorX">The X coordinate where the instance will be created</param>
        /// <param name="coorY">The Y coordinate where the instance will be created</param>
        public Coin(int coorX, int coorY)
        {
            this.BrushLocation = new Uri("../../Resources/ImageBrushes/Coin.png", UriKind.Relative);
            Rect r1 = new Rect(coorX, coorY, 20, 20);
            this.ObjectArea = new RectangleGeometry(r1);
        }
    }
}
﻿namespace CaptainEvil
{
    using System;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Class used to represent brick blocks
    /// </summary>
    public class BrickBlock : BreakableLevelSurface
    {
        /// <summary>
        /// Initializes a new instance of the BrickBlock class.
        /// </summary>
        /// <param name="coorX">The X coordinate where the instance will be created</param>
        /// <param name="coorY">The Y coordinate where the instance will be created</param>
        public BrickBlock(int coorX, int coorY)
        {
            this.BrushLocation = new Uri("../../Resources/ImageBrushes/Bricks.png", UriKind.Relative);
            Rect r1 = new Rect(coorX, coorY, 40, 40);
            this.ObjectArea = new RectangleGeometry(r1);
        }
    }
}
﻿namespace CaptainEvil
{
    using System.IO;

    /// <summary>
    /// Level loader class
    /// </summary>
    public static class LevelLoader
    {
        /// <summary>
        /// Loads a level from a filename onto a model
        /// </summary>
        /// <param name="level_filename">the filename to load the level from</param>
        /// <param name="model">the model to load the level to</param>
        public static void Load(string level_filename, GameModel model)
        {
            string[] lines = File.ReadAllLines(level_filename);
            int ySize = int.Parse(lines[0]);
            int xSize = int.Parse(lines[1]);
            int xPos = 0;
            int yPos = 0;

            for (int x = 0; x < xSize; x++)
            {
                yPos = 0;
                for (int y = 0; y < ySize; y++)
                {
                    if (lines[y + 2][x] == 'G')
                    {
                        model.BaseSurfaces.Add(new GrassBlock(xPos, yPos));
                    }

                    if (lines[y + 2][x] == 'b')
                    {
                        model.BaseSurfaces.Add(new BrickBlock(xPos, yPos));
                    }

                    if (lines[y + 2][x] == 'B')
                    {
                        model.Player = new Bob(xPos, yPos);
                    }

                    if (lines[y + 2][x] == 'M')
                    {
                        model.BaseSurfaces.Add(new MetalBlock(xPos, yPos));
                    }

                    if (lines[y + 2][x] == 'I')
                    {
                        model.BaseSurfaces.Add(new InvisibleBlock(xPos, yPos));
                    }

                    if (lines[y + 2][x] == 'c')
                    {
                        model.Coins.Add(new Coin(xPos, yPos));
                    }

                    yPos = y * 40;
                }

                xPos = x * 40;
            }
        }
    }
}

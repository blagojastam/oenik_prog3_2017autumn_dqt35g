﻿namespace CaptainEvil
{
    using System;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Class used to represent metal blocks
    /// </summary>
    public class MetalBlock : NonBreakableLevelSurface
    {
        /// <summary>
        /// Initializes a new instance of the MetalBlock class.
        /// </summary>
        /// <param name="coorX">The X coordinate where the instance will be created</param>
        /// <param name="coorY">The Y coordinate where the instance will be created</param>
        public MetalBlock(int coorX, int coorY)
        {
            this.BrushLocation = new Uri("../../Resources/ImageBrushes/Metal.png", UriKind.Relative);
            Rect r1 = new Rect(coorX, coorY, 40, 40);
            this.ObjectArea = new RectangleGeometry(r1);
        }
    }
}
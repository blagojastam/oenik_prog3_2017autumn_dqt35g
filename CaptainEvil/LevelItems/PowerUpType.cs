﻿namespace CaptainEvil
{
    /// <summary>
    /// Enumerates the Powerup Types
    /// </summary>
    public enum PowerUpType
    {
        /// <summary>
        /// Does nothing
        /// </summary>
        NOTHING = 0,

        /// <summary>
        /// Provides invincibility for the player
        /// </summary>
        INVINCIBILITY = 1, 

        /// <summary>
        /// Provides a speed increase for the player
        /// </summary>
        SPEEDINCREASE = 2, 

        /// <summary>
        /// Provides damage resilience for the player
        /// </summary>
        DAMAGERESILIENCE = 3, 
        
        /// <summary>
        /// Grants infinite ammo to the player
        /// </summary>
        INFINITEAMMO = 4, 

        /// <summary>
        /// Provides greater damage dealt by the players' weapons
        /// </summary>
        DAMAGEINCREASE = 5
    }
}
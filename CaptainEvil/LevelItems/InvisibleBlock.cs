﻿namespace CaptainEvil
{
    using System;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Class used to represent invisible blocks
    /// </summary>
    public class InvisibleBlock : NonBreakableLevelSurface
    {
        /// <summary>
        /// Initializes a new instance of the InvisibleBlock class.
        /// </summary>
        /// <param name="coorX">The X coordinate where the instance will be created</param>
        /// <param name="coorY">The Y coordinate where the instance will be created</param>
        public InvisibleBlock(int coorX, int coorY)
        {
            this.BrushLocation = new Uri("../../Resources/ImageBrushes/Invisible.png", UriKind.Relative);
            Rect r1 = new Rect(coorX, coorY, 40, 40);
            this.ObjectArea = new RectangleGeometry(r1);
        }
    }
}
﻿namespace CaptainEvil
{
    using System;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Represents the cursor
    /// </summary>
    public class Cursor : GameObject
    {
        /// <summary>
        /// Initializes a new instance of the Cursor class.
        /// </summary>
        public Cursor()
        {
            this.BrushLocation = new Uri("../../Resources/ImageBrushes/Cursor.png", UriKind.Relative);
            GeometryGroup g = new GeometryGroup();
            Rect r1 = new Rect(-5, -5, 15, 15);
            EllipseGeometry eg = new EllipseGeometry(r1);
            g.Children.Add(eg);
            this.ObjectArea = new EllipseGeometry(r1);
        }
    }
}
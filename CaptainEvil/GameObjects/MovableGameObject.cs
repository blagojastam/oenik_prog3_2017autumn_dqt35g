﻿namespace CaptainEvil
{
    using System;
    using System.Threading;

    /// <summary>
    /// Parent class for all moveable (gravitable) game objects
    /// </summary> 
    public abstract class MoveableGameObject : GameObject
    {
        /// <summary>
        /// A boolean value indicating whether gravity should be applied to the GameObject
        /// </summary>
        private bool gravitable = true;

        /// <summary>
        /// Gets or sets whether a value indicating whether the object is gravitable
        /// </summary>
        public bool Gravitable
        {
            get
            {
                return this.gravitable;
            }

            set
            {
                this.gravitable = value;
            }
        }

        /// <summary>
        /// Applies the movement (Delta) to the coordinate of the GameObject
        /// </summary>
        public void ApplyMovementX()
        {
            this.CoordinateX += this.DeltaX;
        }

        /// <summary>
        /// Applies the movement (Delta) to the coordinate of the GameObject
        /// </summary>
        public void ApplyMovementY()
        {
            this.CoordinateY += this.DeltaY;
        }

        /// <summary>
        /// Method called when the object should "jump"
        /// </summary>
        public void Jump()
        {
            if (!this.gravitable)
            {
                Thread jumpThread = new Thread(this.JumpTimeout);
                jumpThread.Name = "JUMPTHREAD";
                jumpThread.Start();
            }

            this.gravitable = true;
        }

        /// <summary>
        /// Gets called at each tick
        /// </summary>
        public void Tick()
        {
            this.ApplyMovementX();
            this.ApplyMovementY();
            this.ApplyGravity();
        }

        /// <summary>
        /// Applies gravity (increase change in Y) to the object if it is gravitable.
        /// </summary>
        public virtual void ApplyGravity()
        {
            this.DeltaY = this.Gravitable ? this.DeltaY + 0.7 : 0;
        }

        /// <summary>
        /// Method that actually does the jumping logic.
        /// </summary>
        private void JumpTimeout()
        {
            double initialSpeed = -3;
            int ticks = 0;
            while (ticks < Config.TICKRATE * 0.4 && initialSpeed < 0)
            {
                Thread.Sleep(TimeSpan.FromMilliseconds(1000 / Config.TICKRATE));
                this.DeltaY += initialSpeed;
                initialSpeed += 50 / Config.TICKRATE;
                ticks++;
                this.ApplyMovementY();
                this.ApplyGravity();
            }

            this.gravitable = true;
        }
    }
}

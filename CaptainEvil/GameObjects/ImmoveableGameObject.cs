﻿namespace CaptainEvil
{
    /// <summary>
    /// Abstract class for inheritance purposes
    /// </summary>
    public abstract class ImmoveableGameObject : GameObject
    {
    }
}

﻿namespace CaptainEvil
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Class used to represent the beginning of the inheritance tree. Cannot be initialized.
    /// </summary>
    public abstract class GameObject 
    {
        /// <summary>
        /// The area of the Object
        /// </summary>
        private Geometry objectArea;

        /// <summary>
        /// X coordinate of the GameObject
        /// </summary>
        private double coordinateX;

        /// <summary>
        /// Y coordinate of the GameObject
        /// </summary>
        private double coordinateY;

        /// <summary>
        /// Represents the change in X coordinate at each tick.
        /// </summary>
        private double deltaX;

        /// <summary>
        /// Represents the change in Y coordinate at each tick.
        /// </summary>
        private double deltaY;

        /// <summary>
        /// The location of the brush to be rendered.
        /// </summary>
        private Uri brushLocation;

        /// <summary>
        /// Gets the RenderedArea of the GameObject using the TransformGroup's methods.
        /// </summary>
        public Geometry ObjectGeometry
        {
            get
            {
                TransformGroup tg = new TransformGroup();
                this.ObjectArea.Transform = tg;
                tg.Children.Add(new TranslateTransform(this.coordinateX, this.coordinateY));
                return this.ObjectArea.GetFlattenedPathGeometry();
            }
        }

        /// <summary>
        /// Gets or sets the Y coordinate of the GameObject
        /// </summary>
        public double CoordinateY
        {
            get
            {
                return this.coordinateY;

            }

            set
            {
                this.coordinateY = value;
            }
        }

        /// <summary>
        /// Gets or sets the X coordinate of the GameObject
        /// </summary>
        public double CoordinateX
        {
            get
            {
                return this.coordinateX;
            }

            set
            {
                this.coordinateX = value;
            }
        }

        /// <summary>
        /// Gets or sets the change in X coordinate of the GameObjects
        /// </summary>
        public double DeltaX
        {
            get
            {
                return this.deltaX;
            }

            set
            {
                this.deltaX = value;
            }
        }

        /// <summary>
        /// Gets or sets the change in Y coordinate of the GameObjects
        /// </summary>
        public double DeltaY
        {
            get
            {
                return this.deltaY;
            }

            set
            {
                this.deltaY = value;
            }
        }

        /// <summary>
        /// Gets the ImageBrush to be rendered.
        /// </summary>
        public ImageBrush Brush
        {
            get
            {
                return new ImageBrush(new BitmapImage(this.BrushLocation));
            }
        }

        /// <summary>
        /// Gets or sets the brush location Uri
        /// </summary>
        public Uri BrushLocation
        {
            get
            {
                return this.brushLocation;
            }

            set
            {
                this.brushLocation = value;
            }
        }

        /// <summary>
        /// Gets or sets the ObjectArea Geometry of the GameObject
        /// </summary>
        protected Geometry ObjectArea
        {
            get
            {
                return this.objectArea;
            }

            set
            {
                this.objectArea = value;
            }
        }

        /// <summary>
        /// Method used to check whether an object collides with an other object
        /// </summary>
        /// <param name="otherObject">The object to check for collision with.</param>
        /// <returns>Returns true if an object has collided with an other object.</returns>
        public bool IsCollision(GameObject otherObject)
        {
            return this.IntersectsWithBottom(otherObject) && this.IntersectsWithLeft(otherObject) && this.IntersectsWithRight(otherObject) && this.IntersectsWithTop(otherObject);
        }

        /// <summary>
        /// Returns the vector between two points
        /// </summary>
        /// <param name="source">Point(x, y) of the source</param>
        /// <param name="target">Point(x, y) of the target</param>
        /// <returns>New Vector instance in directed from the source to the target</returns>
        public Vector GetVector(Point source, Point target)
        {
            return new Vector((target.X - source.X), (target.Y - source.Y));
        }

        /// <summary>
        /// Returns the vector between two GameObjects
        /// </summary>
        /// <param name="source">GameObject source</param>
        /// <param name="target">GameObject target</param>
        /// <returns>New Vector instance in directed from the source to the target</returns>
        public Vector GetVector(GameObject source, GameObject target)
        {
            return this.GetVector(new Point(source.coordinateX, source.CoordinateY), new Point(target.coordinateX, target.CoordinateY)); 
        }

        /// <summary>
        /// Returns the vector between a GameObject and a Point
        /// </summary>
        /// <param name="source">The GameObject of origin</param>
        /// <param name="target">The Point of the target</param>
        /// <returns>New Vector instance in directed from the source to the target</returns>
        public Vector GetVector(GameObject source, Point target)
        {
            return this.GetVector(new Point(source.coordinateX, source.CoordinateY), target);
        }

        /// <summary>
        /// Checks whether the current GameObject instance is within the specified distance of another GameObject.
        /// </summary>
        /// <param name="otherObject">The other GameObject</param>
        /// <param name="distance">The distance to be checked (Radius)</param>
        /// <returns>A boolean value indicating whether the GameObject is in within the specified radius of the other GameObject</returns>
        public bool IsWithin(GameObject otherObject, double distance)
        {
            // ((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)) < d*d
            double x1 = otherObject.ObjectGeometry.Bounds.TopLeft.X;
            double y1 = otherObject.ObjectGeometry.Bounds.TopLeft.Y;
            double x2 = this.ObjectGeometry.Bounds.TopLeft.X;
            double y2 = this.ObjectGeometry.Bounds.TopLeft.Y;

            return ((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2)) < distance * distance;
        }

        /// <summary>
        /// Checks whether the current GameObject's bottom intersects with another GameObject top
        /// </summary>
        /// <param name="otherObject">The other GameObject</param>
        /// <returns>A boolean value indicating whether the objects intersect in the specified manner</returns>
        public bool IntersectsWithBottom(GameObject otherObject)
        {
            if (this.ObjectGeometry.Bounds.Bottom >= otherObject.ObjectGeometry.Bounds.Top)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the current GameObject's top intersects with another GameObject bottom
        /// </summary>
        /// <param name="otherObject">The other GameObject</param>
        /// <returns>A boolean value indicating whether the objects intersect in the specified manner</returns>
        public bool IntersectsWithTop(GameObject otherObject)
        {
            if (this.ObjectGeometry.Bounds.Top <= otherObject.ObjectGeometry.Bounds.Bottom)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the current GameObject's left intersects with another GameObject right
        /// </summary>
        /// <param name="otherObject">The other GameObject</param>
        /// <returns>A boolean value indicating whether the objects intersect in the specified manner</returns>
        public bool IntersectsWithLeft(GameObject otherObject)
        {
            if (this.ObjectGeometry.Bounds.Left <= otherObject.ObjectGeometry.Bounds.Right)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the current GameObject's right intersects with another GameObject left
        /// </summary>
        /// <param name="otherObject">The other GameObject</param>
        /// <returns>A A boolean value indicating whether the objects intersect in the specified manner</returns>
        public bool IntersectsWithRight(GameObject otherObject)
        {
            if (this.ObjectGeometry.Bounds.Right >= otherObject.ObjectGeometry.Bounds.Left)
            {
                return true;
            }

            return false;
        }
    }
}

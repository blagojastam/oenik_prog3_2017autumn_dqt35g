﻿namespace CaptainEvil
{
    /// <summary>
    /// Represents the pistol class
    /// </summary>
    public class Pistol : Weapon
    {
        /// <summary>
        /// Initializes a new instance of the Pistol class.
        /// </summary>
        public Pistol()
        {
            this.MaxMagAmmo = Config.PISTOLMAGAMMO;
            this.CurrentMagazineAmmo = Config.PISTOLMAGAMMO;
        }

    }
}

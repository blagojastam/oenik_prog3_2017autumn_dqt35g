﻿namespace CaptainEvil
{
    using System.Windows;

    /// <summary>
    /// Abstract class for inheritance purporses
    /// </summary>
    public abstract class Weapon : ImmoveableGameObject
    {
        /// <summary>
        /// The maximum number of rounds that the magazine of the particular weapon can hold.
        /// </summary>
        private int maxMagazineAmmo;

        /// <summary>
        /// The current number of rounds in the weapon's magazine.
        /// </summary>
        private int currentMagazineAmmo;

        /// <summary>
        /// A boolean value indicating whether the weapon is selected by the player as the active weapon. 
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// Gets or sets the maximum amount of ammo in the magazine of the weapon.
        /// </summary>
        public int MaxMagAmmo
        {
            get
            {
                return this.maxMagazineAmmo;
            }

            set
            {
                this.maxMagazineAmmo = value;
            }
        }

        /// <summary>
        /// Gets or sets the current amount of ammo in the magazine of the weapon.
        /// </summary>
        public int CurrentMagazineAmmo
        {
            get
            {
                return this.currentMagazineAmmo;
            }

            set
            {
                this.currentMagazineAmmo = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the weapon is currently selected by the player.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                this.isSelected = value;
            }
        }

        /// <summary>
        /// Method that returns whether the current player can shoot the weapon
        /// </summary>
        /// <param name="currentPlayer">The player which is in possession of the weapon</param>
        /// <returns>a boolean indicating whether the player can shoot</returns>
        public bool CanShoot(PlayerCharacter currentPlayer)
        {
            if (this is Pistol)
            {
                if (this.currentMagazineAmmo > 0)
                {
                    return true;
                }
            }

            if (this is Rifle)
            {
                if (this.currentMagazineAmmo > 0)
                {
                    return true;
                }
            }

            return false; 
        }

        /// <summary>
        /// Method that returns whether the current player can reload the weapon
        /// </summary>
        /// <param name="currentPlayer">The player which is in possession of the weapon</param>
        /// <returns>a boolean indicating whether the player can reload</returns>
        public bool CanReload(PlayerCharacter currentPlayer)
        {
            if (this.currentMagazineAmmo == this.maxMagazineAmmo)
            {
                return false;
            }
            else if (this is Pistol && currentPlayer.PistolAmmo == 0)
            {
                return false;
            }
            else if (this is Rifle && currentPlayer.RifleAmmo == 0)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Reloads the weapon and adjusts the ammo in the player's properties
        /// </summary>
        /// <param name="currentPlayer">The player which is in possession of the weapon</param>
        public void Reload(PlayerCharacter currentPlayer)
        {
            if (this is Pistol)
            {
                if (currentPlayer.PistolAmmo >= this.maxMagazineAmmo)
                {
                    currentPlayer.PistolAmmo -= (this.maxMagazineAmmo - this.currentMagazineAmmo);
                    this.currentMagazineAmmo = this.maxMagazineAmmo;                                
                }
                else if (currentPlayer.PistolAmmo > 0 && currentPlayer.PistolAmmo < this.maxMagazineAmmo)
                {
                    if (currentPlayer.PistolAmmo + currentPlayer.SelectedWeapon.currentMagazineAmmo > this.maxMagazineAmmo)
                    {
                        currentPlayer.PistolAmmo -= (this.maxMagazineAmmo - this.currentMagazineAmmo);
                        this.currentMagazineAmmo = this.maxMagazineAmmo;
                    }
                    else
                    {
                        this.currentMagazineAmmo += currentPlayer.PistolAmmo;
                        currentPlayer.PistolAmmo = 0;
                    }
                }
            }

            if (this is Rifle)
            {
                if (currentPlayer.RifleAmmo >= this.maxMagazineAmmo)
                {
                    currentPlayer.RifleAmmo -= (this.maxMagazineAmmo - this.currentMagazineAmmo);
                    this.currentMagazineAmmo = this.maxMagazineAmmo;                         
                }
                else if (currentPlayer.RifleAmmo > 0 && currentPlayer.RifleAmmo < this.maxMagazineAmmo)
                {
                    if (currentPlayer.RifleAmmo + currentPlayer.SelectedWeapon.currentMagazineAmmo > this.maxMagazineAmmo)
                    {
                        currentPlayer.RifleAmmo -= (this.maxMagazineAmmo - this.currentMagazineAmmo);
                        this.currentMagazineAmmo = this.maxMagazineAmmo;
                    }
                    else
                    {
                        this.currentMagazineAmmo += currentPlayer.RifleAmmo;
                        currentPlayer.RifleAmmo = 0;
                    }
                }
            }
        }
        
        /// <summary>
        /// Reload the weapon when the user presses a key
        /// </summary>
        /// <param name="player">the player character</param>
        public void ReloadByUserInput(PlayerCharacter player)
        {
            if (player.SelectedWeapon.CanReload(player))
            {
                player.SelectedWeapon.Reload(player);
            }
        }

        /// <summary>
        /// Shoot the weapon in the direction towards the destination
        /// </summary>
        /// <param name="source">the source of the bullet</param>
        /// <param name="destination">the bullet's destination</param>
        /// <returns>new bullet in the direction towards the destination</returns>
        public Bullet Shoot(PlayerCharacter source, Point destination)
        {
            if (this.CanShoot(source))
            {
                if (source.SelectedWeapon is Rifle)
                {
                    source.SelectedWeapon.currentMagazineAmmo--;
                }

                if (source.SelectedWeapon is Pistol)
                {
                    source.SelectedWeapon.currentMagazineAmmo--;
                }

                Vector direction = GetVector(source.ObjectGeometry.Bounds.TopRight, destination);
                direction.Normalize(); 
                return new Bullet(direction, source);
            }
            else
            {
                if (source.SelectedWeapon.CanReload(source))
                {
                    source.SelectedWeapon.Reload(source);
                }

                return null;
            }
        }
    }
}

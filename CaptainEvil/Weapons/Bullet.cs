﻿namespace CaptainEvil
{
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Bullet class
    /// </summary>
    public class Bullet : MoveableGameObject
    {
        /// <summary>
        /// Initializes a new instance of the Bullet class.
        /// </summary>
        /// <param name="direction">the vector the bullet should be flying at</param>
        /// <param name="sender">the sender of the bullet</param>
        public Bullet(Vector direction, GameObject sender)
        {
            this.DeltaX = direction.X * 10; // CHANGE // ALTER THE SPEED OF THE BULLETS
            this.DeltaY = direction.Y * 10;
            this.CoordinateX = sender.ObjectGeometry.Bounds.TopRight.X;
            this.CoordinateY = sender.ObjectGeometry.Bounds.TopRight.Y;
            GeometryGroup g = new GeometryGroup();
            Rect r1 = new Rect(0, 0, 5, 5);
            EllipseGeometry eg = new EllipseGeometry(r1);
            RectangleGeometry rg = new RectangleGeometry(r1);
            g.Children.Add(eg);
            this.ObjectArea = g.GetWidenedPathGeometry(new Pen(Brushes.Gold, 2));
        }

        /// <summary>
        /// Used to override and not allow bullets to be affected by gravity
        /// </summary>
        public override void ApplyGravity()
        {
            // doing this so no gravity is applied to bullets :/ 
        }
    }
}
﻿namespace CaptainEvil
{
    /// <summary>
    /// Rifle class
    /// </summary>
    public class Rifle : Weapon
    {
        /// <summary>
        /// Initializes a new instance of the Rifle class.
        /// </summary>
        public Rifle()
        {
            this.MaxMagAmmo = 30;
            this.CurrentMagazineAmmo = 30;
        }
    }
}

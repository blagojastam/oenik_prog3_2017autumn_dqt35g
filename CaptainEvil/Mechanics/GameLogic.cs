﻿namespace CaptainEvil
{
    using System.Windows;

    /// <summary>
    /// Class that represents the game's logic
    /// </summary>
    public class GameLogic
    {
        /// <summary>
        /// The model the logic operates with
        /// </summary>
        private GameModel model;

        /// <summary>
        /// Initializes a new instance of the GameLogic class.
        /// </summary>
        /// <param name="newmodel">the model to create the logic for</param>
        public GameLogic(GameModel newmodel)
        {
            this.model = newmodel;
        }

        /// <summary>
        /// Gets called at each tick
        /// </summary>
        public void Tick()
        {
            if (this != null)
            {
                this.InteractItems();
                this.TickAllMovables();
            }
        }

        /// <summary>
        /// Logic wrapper for the Jump method
        /// </summary>
        public void PlayerJump()
        {
            this.model.Player.Jump();
        }

        /// <summary>
        /// Logic wrapper for the Move method
        /// </summary>
        /// <param name="dx">Change in X</param>
        public void MovePlayerX(double dx)
        {
            if (this.model.Player != null)
            {
                this.model.Player.DeltaX = dx;
            }
        }

        /// <summary>
        /// Reloads the player's selected weapon
        /// </summary>
        public void ReloadSelectedPlayerWeapon()
        {
            this.model.Player.SelectedWeapon.ReloadByUserInput(this.model.Player);
        }

        /// <summary>
        /// Selects an other weapon of the player
        /// </summary>
        /// <param name="weaponID">The weapon ID</param>
        public void SelectWeapon(int weaponID)
        {
            this.model.Player.SelectWeapon(weaponID);
        }

        /// <summary>
        /// Wrapper for all the interaction methods
        /// </summary>
        public void InteractItems()
        {
            this.InteractBullets();
            this.InteractGravity();
            this.InteractCoins();
        }

        /// <summary>
        /// Shoots the current weapon at a target (cursor)
        /// </summary>
        /// <param name="target">the target for the shot</param>
        internal void ShootCurrentWeapon(Point target)
        {
            this.model.Bullets.Add(this.model.Player.SelectedWeapon.Shoot(this.model.Player, target));
        }

        /// <summary>
        /// Sets the cursor position
        /// </summary>
        /// <param name="cursor">point (cursor)</param>
        internal void SetCursor(Point cursor)
        {
            this.model.Cursor.CoordinateX = cursor.X;
            this.model.Cursor.CoordinateY = cursor.Y;
        }

        /// <summary>
        /// Interacts all bullets with the surroundings
        /// </summary>
        private void InteractBullets()
        {
            bool removeSurface = false;
            bool removeBullet = false;
            int surfaceIndex = 0;
            int bulletIndex = 0;
            for (int i = 0; i < this.model.BaseSurfaces.Count; i++)
            {
                if (this.model.BaseSurfaces[i] is BreakableLevelSurface)
                {
                    for (int j = 0; j < this.model.Bullets.Count; j++)
                    {
                        if (this.model.Bullets[j] is Bullet)
                        {
                            if (this.model.Bullets[j].IsWithin(this.model.BaseSurfaces[i], 40))
                            {
                                if (this.model.Bullets[j].IsCollision(this.model.BaseSurfaces[i]))
                                {
                                    surfaceIndex = i;
                                    bulletIndex = j;
                                    removeSurface = true;
                                    removeBullet = true;
                                }
                            }
                        }
                    }
                }

                if (this.model.BaseSurfaces[i] is NonBreakableLevelSurface)
                {
                    for (int j = 0; j < this.model.Bullets.Count; j++)
                    {
                        if (this.model.Bullets[j] is Bullet)
                        {
                            if (this.model.Bullets[j].IsWithin(this.model.BaseSurfaces[i], 40))
                            {
                                if (this.model.Bullets[j].IsCollision(this.model.BaseSurfaces[i]))
                                {
                                    surfaceIndex = j;
                                    removeBullet = true;
                                }
                            }
                        }
                    }
                }
            }

            for (int j = 0; j < this.model.Bullets.Count; j++)
            {
                if (this.model.Bullets[j] is Bullet)
                {
                    if (!this.model.Bullets[j].IsWithin(this.model.Player, 1000))
                    {
                        bulletIndex = j;
                        removeBullet = true;
                    }
                }
            }

            if (removeSurface)
            {
                if (removeBullet)
                {
                    this.model.Bullets.Remove(this.model.Bullets[bulletIndex]);
                }
                this.model.BaseSurfaces.Remove(this.model.BaseSurfaces[surfaceIndex]);
            }
            else if (removeBullet)
            {
                this.model.Bullets.Remove(this.model.Bullets[bulletIndex]);
            }
        }

        /// <summary>
        /// Interacts the player with the gravity.
        /// </summary>
        private void InteractGravity()
        {
            foreach (LevelSurface surface in this.model.BaseSurfaces)
            {
                if (this.model.Player.IsWithin(surface, 80))
                {
                    if (surface.ObjectGeometry.Bounds.Top < this.model.Player.ObjectGeometry.Bounds.Bottom)
                    {
                        this.model.Player.DeltaY = 0;
                        this.model.Player.Gravitable = false;
                        this.model.Player.CoordinateY = surface.ObjectGeometry.Bounds.Top - 360;
                        return;
                    }
                    this.model.Player.Gravitable = false;
                }
            }
        }

        /// <summary>
        /// Calls the tick method for every movable item
        /// </summary>
        private void TickAllMovables()
        {
            if (this.model.Player != null)
            {
                this.model.Player.Tick();
            }
            foreach (Bullet item in this.model.Bullets)
            {
                if (item != null && item.IsWithin(this.model.Player, 1000))
                {
                    item.Tick();
                }
            }
        }

        /// <summary>
        /// Interacts the coins with the player
        /// </summary>
        private void InteractCoins()
        {
            int coinIndex = 0;
            bool remove = false;
            for (int i = 0; i < this.model.Coins.Count; i++)
            {
                if (this.model.Player.IsCollision(this.model.Coins[i]))
                {
                    this.model.Player.Coins.Add(this.model.Coins[i]);
                    coinIndex = i;
                    remove = true;
                }
            }
            if (remove)
            {
                this.model.Coins.Remove(this.model.Coins[coinIndex]);
            }
        }
    }
}

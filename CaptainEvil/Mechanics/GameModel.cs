﻿namespace CaptainEvil
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Class that represents the model
    /// </summary>
    public class GameModel 
    {
        /// <summary>
        /// The player character
        /// </summary>
        private PlayerCharacter player;

        /// <summary>
        /// The cursor in the window
        /// </summary>
        private Cursor cursor;

        /// <summary>
        /// A list of all the surfaces 
        /// </summary>
        private List<LevelSurface> baseSurfaces;

        /// <summary>
        /// A list of all the bullets
        /// </summary>
        private List<Bullet> bullets;

        /// <summary>
        /// A list of all the powerups
        /// </summary>
        private List<PowerUp> powerUps;

        /// <summary>
        /// A list of all the NPCs
        /// </summary>
        private List<NPC> characters;

        /// <summary>
        /// A list of all the weapons
        /// </summary>
        private List<Weapon> weapons;

        /// <summary>
        /// A list of all the coins
        /// </summary>
        private List<Coin> coins;

        /// <summary>
        /// The background image
        /// </summary>
        private ImageBrush background;

        /// <summary>
        /// Initializes a new instance of the GameModel class.
        /// </summary>
        public GameModel()
        {
            this.Cursor = new Cursor();
            this.BaseSurfaces = new List<LevelSurface>();
            this.Bullets = new List<Bullet>();
            this.powerUps = new List<PowerUp>();
            this.characters = new List<NPC>();
            this.weapons = new List<Weapon>();
            this.coins = new List<Coin>();
            this.background = new ImageBrush(new BitmapImage(new Uri("../../Resources/ImageBrushes/Background.jpg", UriKind.Relative)));
            LevelLoader.Load("../../Resources/Levels/level1.txt", this);
        }

        /// <summary>
        /// Gets or sets the player character
        /// </summary>
        public PlayerCharacter Player
        {
            get
            {
                return this.player;
            }

            set
            {
                this.player = value;
            }
        }

        /// <summary>
        /// Gets or sets the cursor
        /// </summary>
        public Cursor Cursor
        {
            get
            {
                return this.cursor;
            }

            set
            {
                this.cursor = value;
            }
        }

        /// <summary>
        /// Gets or sets the list of surfaces
        /// </summary>
        public List<LevelSurface> BaseSurfaces
        {
            get
            {
                return this.baseSurfaces;
            }

            set
            {
                this.baseSurfaces = value;
            }
        }

        /// <summary>
        /// Gets or sets the list of bullets
        /// </summary>
        public List<Bullet> Bullets
        {
            get
            {
                return this.bullets;
            }

            set
            {
                this.bullets = value;
            }
        }

        /// <summary>
        /// Gets or sets the list of coins
        /// </summary>
        public List<Coin> Coins
        {
            get
            {
                return this.coins;
            }

            set
            {
                this.coins = value;
            }
        }

        /// <summary>
        /// Gets the background image
        /// </summary>
        public ImageBrush Background
        {
            get
            {
                return this.background;
            }
        }
    }
}

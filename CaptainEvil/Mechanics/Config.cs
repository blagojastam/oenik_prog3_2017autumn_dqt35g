﻿namespace CaptainEvil
{
    /// <summary>
    /// Class used to store config variables 
    /// </summary>
    public static class Config
    {
        /// <summary>
        /// The tick rate of the game expressed in ticks per second.
        /// </summary>
        public const double TICKRATE = 128;

        /// <summary>
        /// The width of the window expressed in pixels.
        /// </summary>
        public const int WINDOWHEIGHT = 450;

        /// <summary>
        /// The height of the window expressed in pixels.
        /// </summary>
        public const int WINDOWWIDTH = 800;

        /// <summary>
        /// Global gravity
        /// </summary>
        public const double GRAVITY = 0.981;

        /// <summary>
        /// Pistol's fire rate in RPM
        /// </summary>
        public const double PISTOLFIRERATE = 240 * 60 / TICKRATE;

        /// <summary>
        /// The maximum ammo capacity of the pistol's magazine
        /// </summary>
        public const int PISTOLMAGAMMO = 20;

        /// <summary>
        /// The damage inflicted by the pistol's bullet
        /// </summary>
        public const int PISTOLDAMAGE = 50;

        /// <summary>
        /// Rifle's fire rate in RPM
        /// </summary>
        public const double RIFLEFIRERATE = 667 * 60 / TICKRATE;

        /// <summary>
        /// The maximum ammo capacity of the rifle's magazine
        /// </summary>
        public const int RIFLEMAGAMMO = 30;

        /// <summary>
        /// The damage inflicted by the rifle's bullet
        /// </summary>
        public const int RIFLEDAMAGE = 75;

        /// <summary>
        /// Bob's health
        /// </summary>
        public const int BOBHEALTH = 120;

        /// <summary>
        /// Bob's maximum speed
        /// </summary>
        public const double BOBSPEED = 7;
    }
}

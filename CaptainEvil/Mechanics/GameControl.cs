﻿namespace CaptainEvil
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;

    /// <summary>
    /// Control of the game's behavior
    /// </summary>
    public class GameControl : FrameworkElement
    {
        /// <summary>
        /// The game's model
        /// </summary>
        private GameModel model;

        /// <summary>
        /// The game's logic
        /// </summary>
        private GameLogic logic;

        /// <summary>
        /// DispatcherTimer used to control the game's ticks
        /// </summary>
        private DispatcherTimer timer;

        /// <summary>
        /// Initializes a new instance of the GameControl class.
        /// </summary>
        public GameControl()
        {
            this.Loaded += this.GameControl_Loaded;
        }

        /// <summary>
        /// Method that renders things on the screen
        /// </summary>
        /// <param name="drawingContext">drawing context</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            Pen p = new Pen(Brushes.Transparent, 0);
            if (this.model != null)
            {
                drawingContext.DrawGeometry(this.model.Background, p, new RectangleGeometry(new Rect(0, 0, 800, 475)));
                foreach (Bullet item in this.model.Bullets)
                {
                    if (item != null && item.IsWithin(this.model.Player, 1000))
                    {
                        drawingContext.DrawGeometry(Brushes.Gold, new Pen(Brushes.Gold, 2), item.ObjectGeometry);
                    }
                }

                foreach (LevelSurface item in this.model.BaseSurfaces)
                {
                    if (item != null)
                    {
                        drawingContext.DrawGeometry(item.Brush, p, item.ObjectGeometry);
                    }
                }

                drawingContext.DrawGeometry(this.model.Player.Brush, p, this.model.Player.ObjectGeometry);
                drawingContext.DrawGeometry(this.model.Cursor.Brush, p, this.model.Cursor.ObjectGeometry);
                foreach (Coin item in this.model.Coins)
                {
                    if (item != null)
                    {
                        drawingContext.DrawGeometry(item.Brush, p, item.ObjectGeometry);
                    }
                }

                if (this.model.Player.SelectedWeapon is Pistol)
                {
                    FormattedText currentmagammo = new FormattedText("Pistol Ammo: " + this.model.Player.SelectedWeapon.CurrentMagazineAmmo + "/" + this.model.Player.PistolAmmo, System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 30, Brushes.Red);
                    drawingContext.DrawText(currentmagammo, new Point(50, 50));
                }

                if (this.model.Player.SelectedWeapon is Rifle)
                {
                    FormattedText currentmagammo = new FormattedText("Rifle Ammo: " + this.model.Player.SelectedWeapon.CurrentMagazineAmmo + "/" + this.model.Player.RifleAmmo, System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 30, Brushes.Red);
                    drawingContext.DrawText(currentmagammo, new Point(50, 50));
                }

                FormattedText playercoins = new FormattedText("Coins: " + this.model.Player.Coins.Count.ToString(), System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 30, Brushes.Red);
                drawingContext.DrawText(playercoins, new Point(50, 80));
            }
        }

        /// <summary>
        /// Fires when the window is loaded
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event arguments</param>
        private void GameControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.model = new GameModel(); // EDIT // USE TEXTFILE 
            this.logic = new GameLogic(this.model); // SET CONSTRUCTOR
            Window win = Window.GetWindow(this);
            if (win != null)
            {
                win.MouseLeftButtonDown += this.OnLMBClick;
                this.timer = new DispatcherTimer();
                this.timer.Interval = TimeSpan.FromMilliseconds(1000 / Config.TICKRATE);
                this.timer.Tick += this.Tick;
                this.timer.Start();
            }

            this.InvalidateVisual();
        }

        /// <summary>
        /// Fires when the left mouse button is pressed
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event arguments</param>
        private void OnLMBClick(object sender, MouseButtonEventArgs e)
        {
            this.logic.ShootCurrentWeapon(e.GetPosition(Application.Current.MainWindow));
        }

        /// <summary>
        /// Updates the cursor's position
        /// </summary>
        private void UpdateCursorPoisition()
        {
            this.logic.SetCursor(Mouse.GetPosition(Application.Current.MainWindow));
        }

        /// <summary>
        /// Fires when a key is pressed
        /// </summary>
        private void GetKeyPress()
        {
            if (Keyboard.IsKeyDown(Key.D))
            {
                this.logic.MovePlayerX(this.model.Player.MaxSpeed); 
            }

            if (Keyboard.IsKeyDown(Key.A))
            {
                this.logic.MovePlayerX(this.model.Player.MaxSpeed * -1); 
            }

            if (Keyboard.IsKeyDown(Key.R))
            {
                this.logic.ReloadSelectedPlayerWeapon();
            }

            if (Keyboard.IsKeyDown(Key.D1))
            {
                this.logic.SelectWeapon(1);
            }

            if (Keyboard.IsKeyDown(Key.D2))
            {
                this.logic.SelectWeapon(2);
            }

            if (!(Keyboard.IsKeyDown(Key.D) || Keyboard.IsKeyDown(Key.A)))
            {
                this.logic.MovePlayerX(0); // no movement 
            }

            if (Keyboard.IsKeyDown(Key.Space))
            {
                this.logic.PlayerJump();
            }
        }

        /// <summary>
        /// Fires at each game tick
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event arguments</param>
        private void Tick(object sender, EventArgs e)
        {
            this.UpdateCursorPoisition();
            this.GetKeyPress();
            this.logic.Tick();
            this.InvalidateVisual();
        }
    }
}